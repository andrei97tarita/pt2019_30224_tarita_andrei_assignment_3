package BusinessLogic;

import DataAccess.CustomerDAObject;
import model.Customer;

public class CustomerLogic {

	public void newCustomer(Customer customer) {
		CustomerDAObject customerDAObject = new CustomerDAObject();
		customerDAObject.addCustomer(customer);
	}

	public void deleteCustomer(int id) {
		CustomerDAObject customerDAObject = new CustomerDAObject();
		customerDAObject.deleteCustomer(id);
	}
	
	public void updateTelOfCustomer(int id, int tel) {
		CustomerDAObject customerDAObject = new CustomerDAObject();
		customerDAObject.updateCustomerDetails(id, tel);
	}

	public void showCustomer(int id) {
		CustomerDAObject customerDAObject = new CustomerDAObject();
		Customer customer = customerDAObject.getCustomer(id);

		if (customer != null) {
			System.out.println(customer.getName());
		} else {
			System.out.println("Error showing the customer!");
		}
	}

}
