package BusinessLogic;

import DataAccess.ProductDAObject;
import model.Product;

public class ProductLogic {

	public void newProduct(Product product) {
		ProductDAObject productDAObject = new ProductDAObject();
		productDAObject.addProduct(product);
	}
	
	public void deleteProduct(int id) {
		ProductDAObject productDAObject = new ProductDAObject();
		productDAObject.deleteProduct(id);
	}
	
	public void updateStockOfProduct(int id, int stock) {
		ProductDAObject productDAObject = new ProductDAObject();
		productDAObject.updateProductDetails(id, stock);
	}
	
	public void showProduct(int id) {
		ProductDAObject productDAObject = new ProductDAObject();
		Product product = productDAObject.getProduct(id);
		
		if (product != null) {
			System.out.println(product.getName());
		} else {
			System.out.println("Error showing the product!");
		}
	}
}
