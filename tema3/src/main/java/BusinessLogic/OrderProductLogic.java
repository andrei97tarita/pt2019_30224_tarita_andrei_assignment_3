package BusinessLogic;

import java.util.List;

import DataAccess.OrderProductDAObject;
import model.Order;
import model.OrderProduct;

public class OrderProductLogic {

	public void newOrderProduct(int customerID, List<OrderProduct> orderProduct) {
		OrderProductDAObject orderProductDAObject = new OrderProductDAObject();
		orderProductDAObject.addOrder(customerID, orderProduct);
	}
	
}
