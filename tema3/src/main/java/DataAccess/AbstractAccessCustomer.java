package DataAccess;

import model.Customer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AbstractAccessCustomer<T> {

    private final Class<T> type;

    public AbstractAccessCustomer(Class<T> entityClass) {
        this.type = entityClass;
    }

    private String select(String field) {
        return "select * from " + type.getSimpleName() + " where " + field + " = ?";
    }

    private T createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        try {
            while (resultSet.next()) {
                T instance = type.getDeclaredConstructor().newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
           
                return instance;
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T getColumnByID(int id) {
        Connection connection = DBConnection.getConnection();
        ResultSet resultSet = null;
        try {
            PreparedStatement statement = connection.prepareStatement(select("customerID"));
            statement.setInt(1, id);
            statement.execute();
            resultSet = statement.getResultSet();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        //System.out.println(createObjects(resultSet).toString());
        T obj = createObjects(resultSet);
        
        return obj != null ? obj : null;
      }

    public static void main(String[] args) {
        AbstractAccessCustomer<Customer> t = new AbstractAccessCustomer<Customer>(Customer.class);

        Customer p = t.getColumnByID(1);
        if(p != null)
        	System.out.println(p.getCustomerID() + " " + p.getName() + " ");

    }
}