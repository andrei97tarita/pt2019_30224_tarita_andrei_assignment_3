package DataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeMap;

import com.mysql.cj.jdbc.PreparedStatement;

import model.Product;

public class ProductDAObject {

	public static final String PRODUCT_TABLE = "product";
	public static final String COL_ID = "id";
	public static final String COL_NAME = "name";
	public static final String COL_PRICE = "price";
	public static final String COL_QUANTITY = "quantity";
	public static final String COL_STOCK = "stock";

	private String insertProductInQuery = "insert into product(id, name, price, quantity, stock) values (?, ?, ?, ?, ?);";
	private String deleteProductFromQuery = "delete from product where id = ?;";
	private String selectProduct = "select * from product where id = ?;";
	private String selectProducts = "select * from product;";
	private String updateStock = "update product set stock = ? where id = ?;";

	public DBConnection database = new DBConnection();

	public ProductDAObject() {
		// TODO Auto-generated constructor stub
		this.database = new DBConnection();
	}

	public void addProduct(Product product) {

		Connection conn = DBConnection.getConnection();

		java.sql.PreparedStatement findStatement = null;
		try {
			findStatement = conn.prepareStatement(insertProductInQuery);
			findStatement.setInt(1, product.getId());
			findStatement.setString(2, product.getName());
			findStatement.setInt(3, product.getPrice());
			findStatement.setInt(4, product.getQuantity());
			findStatement.setInt(5, product.getStock());
			findStatement.execute();
			System.out.println("Succesfuly added product");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error while adding product!");
			e.printStackTrace();
		}

		try {
			findStatement.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteProduct(int id) {

		Connection conn = DBConnection.getConnection();

		PreparedStatement findStatement = null;
		try {
			findStatement = (PreparedStatement) conn.prepareStatement(deleteProductFromQuery);
			findStatement.setInt(1, id);
			findStatement.executeUpdate();
			System.out.println("Product deleted succesfuly!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error while removing product!");
			e.printStackTrace();
		} finally {
			DBConnection.close(findStatement);
			database.close(conn);
		}
	}

	public void updateProductDetails(int id, int stock) {

		Connection conn = DBConnection.getConnection();
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement(updateStock);
			findStatement.setInt(1, stock);
			findStatement.setInt(2, id);
			findStatement.execute();
			System.out.println("Product updated succesfuly!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(findStatement);
			database.close(conn);
		}
	}

	public Product getProduct(int id) {
		Connection conn = DBConnection.getConnection();
		PreparedStatement findStatement = null;
		ResultSet resultSet = null;
		Product product = null;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement(selectProduct);
			findStatement.setInt(1, id);
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				product = new Product(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3),
						resultSet.getInt(4), resultSet.getInt(5));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error getting product!");
			e.printStackTrace();
		} finally {
			DBConnection.close(resultSet);
			DBConnection.close(findStatement);
			database.close(conn);
		}

		return product;
	}

	public int getNumberOfProducts() {

		Connection conn = DBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;
		int nr = 0;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement("select * from product;");
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				nr++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}

		return nr;
	}
	
	public TreeMap<Integer, Product> getProducts() {

		Connection conn = database.createConnection();
		PreparedStatement findStatement = null;
		ResultSet resultSet = null;
		TreeMap<Integer, Product> products = new TreeMap<Integer, Product>();
		try {
			findStatement = (PreparedStatement) conn.prepareStatement(selectProducts);
			resultSet = findStatement.executeQuery();
			while (resultSet.next()) {
				Product product = new Product(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3),
						resultSet.getInt(4), resultSet.getInt(5));
				products.put(product.getId(), product);
			}
		} catch (SQLException e) {
			System.out.println("Error getting the products!");
			e.printStackTrace();
		} finally {
			DBConnection.close(resultSet);
			DBConnection.close(findStatement);
			database.close(conn);
		}

		return products;
	}
}