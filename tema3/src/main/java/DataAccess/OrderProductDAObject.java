package DataAccess;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import model.OrderProduct;
import model.Product;

public class OrderProductDAObject {

	private String insertOrderInQuery = "insert into _order (cstID, price) values (?, ?);";
	private String selectLastOrder = "select count(*) from _order";
	private String insertOrderProductInQuery = "insert into orderproduct (orderID, productID, quantity) values (?, ?, ?);";
	private String updateStockOfProductInProductTable = "update product set stock = stock - ? where id = ?";
	private String updateTotalPriceOfOrder = "update _order set price = ? where id = ?;";

	public DBConnection database = new DBConnection();

	public OrderProductDAObject() {
		// TODO Auto-generated constructor stub
		this.database = new DBConnection();
	}

	public void addOrder(int customerID, List<OrderProduct> orderProduct) {

		int oID = 0, totalPrice = 0;

		Connection conn = DBConnection.getConnection();

		java.sql.PreparedStatement findStatement = null;
		java.sql.PreparedStatement find2Statement = null;
		java.sql.PreparedStatement find3Statement = null;
		ResultSet resultSet = null;
		try {
			findStatement = conn.prepareStatement(insertOrderInQuery);
			findStatement.setInt(1, customerID);
			findStatement.setInt(2, 0);
			findStatement.execute();

			find2Statement = conn.prepareStatement(selectLastOrder);
			resultSet = find2Statement.executeQuery();
			while (resultSet.next()) {
				oID = resultSet.getInt(1);
			}

			find2Statement = conn.prepareStatement(insertOrderProductInQuery);
			for (OrderProduct op : orderProduct) {
				op.setId(oID);
				find2Statement.setInt(1, oID);
				find2Statement.setInt(2, op.getProductID());
				find2Statement.setInt(3, op.getQuantity());
				find2Statement.execute();
				find3Statement = conn.prepareStatement(updateStockOfProductInProductTable);
				find3Statement.setInt(1, op.getQuantity());
				find3Statement.setInt(2, op.getProductID());
				find3Statement.execute();
				totalPrice = op.getQuantity() * (new ProductDAObject().getProduct(op.getProductID()).getPrice());
				System.out.println(totalPrice);
			}

			findStatement = conn.prepareStatement(updateTotalPriceOfOrder);
			findStatement.setInt(1, totalPrice);
			findStatement.setInt(2, oID);
			findStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createBill(int clientID, List<OrderProduct> orderProduct) {

		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("filename.txt"), "utf-8"));
			writer.write("CustomerID: " + clientID + "\n");
			int totalFactura = 0;
			for (OrderProduct order : orderProduct) {
				Product product = new AbstractAccess<>(Product.class).getColumnByID(order.getProductID());
				writer.write("\t" + "Product: " + product.getName() + "\t" + product.getQuantity() + " x "
						+ order.getQuantity() + "\n");
				totalFactura += product.getQuantity() * order.getQuantity();
				writer.write("\n");
			}
			writer.write("\n------------- Total factura: " + totalFactura + "\n");
		} catch (IOException ex) {
			// Report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}

	}
}
