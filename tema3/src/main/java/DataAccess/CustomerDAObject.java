package DataAccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import com.mysql.cj.jdbc.PreparedStatement;

import DataAccess.DBConnection;
import model.Customer;

public class CustomerDAObject {

	public static final String CUSTOMER_TABLE = "customer";
	public static final String COL_ID = "customerID";
	public static final String COL_NAME = "name";
	public static final String COL_SURRNAME = "surrname";
	public static final String COL_EMAIL = "email";
	public static final String COL_TEL = "tel";

	private String insertCustomerInQuery = "insert into customer values (?, ?, ?, ?, ?);";
	private String selectCustomer = "select * from customer where customerID = ?";
	private String deleteCustomerFromQueue = "delete from customer where customerID = ?";
	private String updateTel = "update customer set tel = ? where customerID = ?;";

	public DBConnection database = new DBConnection();

	public CustomerDAObject() {
		// TODO Auto-generated constructor stub
		this.database = new DBConnection();
	}

	public void addCustomer(Customer customer) {

		Connection conn = DBConnection.getConnection();

		java.sql.PreparedStatement findStatement = null;
		try {
			findStatement = conn.prepareStatement(insertCustomerInQuery);
			findStatement.setInt(1, customer.getCustomerID());
			findStatement.setString(2, customer.getName());
			findStatement.setString(3, customer.getSurrname());
			findStatement.setString(4, customer.getEmail());
			findStatement.setLong(5, customer.getTel());
			findStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			findStatement.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteCustomer(int id) {

		Connection conn = DBConnection.getConnection();

		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement(deleteCustomerFromQueue);
			findStatement.setInt(1, id);
			findStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			findStatement.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updateCustomerDetails(int id, int tel) {

		Connection conn = DBConnection.getConnection();
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement(updateTel);
			findStatement.setInt(1, tel);
			findStatement.setInt(2, id);
			findStatement.execute();
			System.out.println("Customer updated succesfuly!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(findStatement);
			database.close(conn);
		}
	}

	public Customer getCustomer(int id) {
		Connection conn = DBConnection.getConnection();
		Customer customer = null;
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement(selectCustomer);
			findStatement.setInt(1, id);
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				customer = new Customer(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getLong(5));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}
		return customer;
	}

	public int getNumberOfCustomers() {

		Connection conn = DBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;
		int nr = 0;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement("select * from customer;");
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				nr++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}

		return nr;
	}

	public Vector<Vector<String>> getCustomers() {

		Connection conn = DBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;
		Vector<Vector<String>> clienti = new Vector<Vector<String>>();

		try {
			findStatement = (PreparedStatement) conn.prepareStatement("select * from customer;");
			//findStatement.execute();	
			resultSet = findStatement.executeQuery();
			while (resultSet.next()) {
				Vector<String> singlevector = new Vector<String>();
				singlevector.add(resultSet.getString(1));
				singlevector.add(resultSet.getString(2));
				singlevector.add(resultSet.getString(3));
				singlevector.add(resultSet.getString(4));
				singlevector.add(resultSet.getString(5));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}

		return clienti;
	}
}
