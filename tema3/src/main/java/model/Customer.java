package model;

public class Customer {

	private int customerID;
	private String name;
	private String surrname;
	private String email;
	private long tel;

	public Customer(int id, String name, String surrname, String email, long tel) {
		// TODO Auto-generated constructor stub
		this.setCustomerID(id);
		this.setName(name);
		this.setSurrname(surrname);
		this.setEmail(email);
		this.setTel(tel);
	}
	
	public Customer() {
		
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int id) {
		this.customerID = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurrname() {
		return surrname;
	}

	public void setSurrname(String surrname) {
		this.surrname = surrname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getTel() {
		return tel;
	}

	public void setTel(long l) {
		this.tel = l;
	}

	public String toString() {
		return this.customerID + " | " + this.name + " | " + this.surrname + " | " + this.email + " | " + this.tel + " | ";
	}
}
