package model;

public class Product {

	private int id;
	private String name;
	private int stock;
	private int price;
	private int quantity;

	public Product(int id, String name, int stock, int price, int cantity) {
		// TODO Auto-generated constructor stub
		this.setId(id);
		this.setName(name);
		this.setStock(stock);
		this.setQuantity(cantity);
		this.setPrice(price);
	}

	public Product() {
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
