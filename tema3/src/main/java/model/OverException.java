package model;

@SuppressWarnings("serial")
public class OverException extends Exception {

	public OverException() {
		super("Cantitatea a atins limita maxima!");
	}

	public OverException(String message) {
		super(message);
	}

}
