package model;

public class Order {

	private int id;
	private int cstID;
	private int price;

	public Order(int id, int cstID, int price) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.cstID = cstID;
		this.price = price;
	}

	public int getCustomerID() {
		return cstID;
	}

	public int getOrderID() {
		return id;
	}

	public void setCustomerID(int customerID) {
		this.cstID = customerID;
	}

	public void setOrderID(int orderID) {
		this.id = orderID;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
