package model;

public class OrderProduct {

	private int id;
	private int orderID;
	private int productID;
	private int quantity;

	public OrderProduct(int id, int orderID, int productID, int quantity) {
		// TODO Auto-generated constructor stub
		this.setId(id);
		this.setOrderID(orderID);
		this.setProductID(productID);
		this.setQuantity(quantity);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}