package model;

@SuppressWarnings("serial")
public class UnderException extends Exception {

	public UnderException() {
		super("Cantitatea insuficienta!");
	}

	public UnderException(String message) {
		super(message);
	}
}
