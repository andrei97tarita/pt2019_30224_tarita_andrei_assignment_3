package model;

@SuppressWarnings("serial")
public class NotAvailableException extends Exception {

	public NotAvailableException() {
		super("Produsul nu este disponibil!");
	}

	public NotAvailableException(String message) {
		super(message);
	}
}
