package model;

import java.util.Map;
import java.util.TreeMap;

public class Deposit {

	private TreeMap<Integer, Product> deposit = new TreeMap<Integer, Product>();
	private int depositSpace;

	public Deposit(TreeMap<Integer, Product> deposit, int depositSpace) {
		// TODO Auto-generated constructor stub
		this.deposit = deposit;
		this.depositSpace = depositSpace;
	}

	public TreeMap<Integer, Product> getDeposit() {
		return deposit;
	}

	public void setDeposit(TreeMap<Integer, Product> deposit) {
		this.deposit = deposit;
	}

	public int getDepositSpace() {
		return depositSpace;
	}

	public void setDepositSpace(int depositSpace) {
		this.depositSpace = depositSpace;
	}

	public int getActualSpace() {

		int sum = 0;
		for (Map.Entry<Integer, Product> entry : deposit.entrySet()) {
			sum += entry.getValue().getStock();
		}
		return sum;
	}

	public int getPriceOfProduct(String name) {

		for (Map.Entry<Integer, Product> entry : deposit.entrySet()) {
			if (entry.getValue().getName().equals(name)) {
				return entry.getValue().getPrice();
			}
		}
		return 0;
	}

	public boolean isAvailable(Product product) {

		if (deposit.get(product.getId()) != null) {
			return true;
		}

		return false;
	}

	public void updateStock(int id, int cantity) throws NotAvailableException, UnderException {

		if (!this.deposit.containsKey(id)) {
			throw new NotAvailableException();
		} else {
			Product product = deposit.get(id);
			if (product.getQuantity() < cantity) {
				throw new UnderException();
			} else {
				product.setQuantity(product.getQuantity() - cantity);
			}
		}
	}

	public void addProductInDeposit(Product product) throws OverException {

		if (this.getActualSpace() + product.getStock() > this.depositSpace) {
			throw new OverException();
		} else {
			if (deposit.containsKey(product.getId())) {

				deposit.put(product.getId(), product);
			}

		}
	}

	public void removeProductFromDeposit(int id) throws UnderException {

		if (!deposit.containsKey(id)) {
			throw new UnderException();
		} else {
			for (Map.Entry<Integer, Product> entry : deposit.entrySet()) {
				if (id == entry.getKey()) {
					deposit.remove(entry.getKey());
				}
			}
		}
	}
}
