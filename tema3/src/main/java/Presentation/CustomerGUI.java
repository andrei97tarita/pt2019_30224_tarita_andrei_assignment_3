package Presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;

import BusinessLogic.CustomerLogic;
import BusinessLogic.ProductLogic;
import DataAccess.*;
import model.Customer;
import model.OrderProduct;
import model.Product;

public class CustomerGUI {

	JTextField adminTF = new JTextField();
	JTextField passTF = new JTextField();
	private JFrame frame;
	private JTextField idTF = new JTextField();
	private JTextField nameTF = new JTextField();
	private JTextField surrnameTF = new JTextField();
	private JTextField emailTF = new JTextField();
	private JTextField telTF = new JTextField();
	private JFrame createCustomerFrame = new JFrame();
	private JFrame deleteCustomerFrame = new JFrame();
	private JFrame updateCustomerFrame = new JFrame();
	private JFrame listAllCustomers = new JFrame();
	private JFrame listAllProducts = new JFrame();
	private JFrame createProductFrame = new JFrame();
	private JFrame deleteProductFrame = new JFrame();
	private JFrame updateProductFrame = new JFrame();
	private JTextField idProdTF = new JTextField();
	private JTextField nameProdTF = new JTextField();
	private JTextField priceProdTF = new JTextField();
	private JTextField stockProdTF = new JTextField();
	private JTextField quantityProdTF = new JTextField();

	private CustomerLogic customerLogic = new CustomerLogic();
	private ProductLogic productLogic = new ProductLogic();

	private List<OrderProduct> orderProducts = new ArrayList<OrderProduct>();
	private JFrame orderFrame = new JFrame();
	private JTable productsTable;
	private JTable customersTable;

	private void init() {

		frame = new JFrame();
		frame.setBounds(150, 150, 650, 180);
		frame.setBackground(Color.DARK_GRAY);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setName("GUI");
		frame.setVisible(true);

		JButton btnCreateCustomer = new JButton("addClient");
		btnCreateCustomer.setBounds(20, 10, 100, 20);
		frame.add(btnCreateCustomer);

		JButton btnDeleteCustomer = new JButton("delClient");
		btnDeleteCustomer.setBounds(170, 10, 100, 20);
		frame.add(btnDeleteCustomer);

		JButton btnUpdateCustomer = new JButton("upCustomer");
		btnUpdateCustomer.setBounds(320, 10, 120, 20);
		frame.add(btnUpdateCustomer);

		JButton btnListAllCustomers = new JButton("viewCustomers");
		btnListAllCustomers.setBounds(470, 10, 130, 20);
		frame.add(btnListAllCustomers);

		JButton btnUpdateProduct = new JButton("upProduct");
		btnUpdateProduct.setBounds(320, 50, 120, 20);
		frame.add(btnUpdateProduct);

		JButton btnListAllProducts = new JButton("viewProducts");
		btnListAllProducts.setBounds(470, 50, 130, 20);
		frame.add(btnListAllProducts);

		JButton btnOrders = new JButton("Order");
		btnOrders.setBounds(20, 90, 100, 20);
		frame.add(btnOrders);

		btnCreateCustomer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				createCustomerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				createCustomerFrame.setBounds(200, 200, 600, 250);
				createCustomerFrame.setResizable(false);
				createCustomerFrame.setVisible(true);
				createCustomerFrame.setLayout(null);

				JLabel idLabel = new JLabel("id: ");
				idLabel.setBounds(0, 10, 80, 20);
				idLabel.setVisible(true);
				idTF.setBounds(80, 10, 200, 20);
				idTF.setVisible(true);
				JLabel nameLabel = new JLabel("name: ");
				nameLabel.setBounds(0, 50, 80, 20);
				nameLabel.setVisible(true);
				nameTF.setBounds(80, 50, 200, 20);
				nameTF.setVisible(true);
				JLabel surrnameLabel = new JLabel("surrname: ");
				surrnameLabel.setBounds(0, 90, 80, 20);
				surrnameLabel.setVisible(true);
				surrnameTF.setBounds(80, 90, 200, 20);
				surrnameTF.setVisible(true);
				JLabel emailLabel = new JLabel("email: ");
				emailLabel.setBounds(0, 130, 80, 20);
				emailLabel.setVisible(true);
				emailTF.setBounds(80, 130, 200, 20);
				emailTF.setVisible(true);
				JLabel telLabel = new JLabel("tel: ");
				telLabel.setBounds(0, 170, 80, 20);
				telLabel.setVisible(true);
				telTF.setBounds(80, 170, 200, 20);
				telTF.setVisible(true);

				JButton create = new JButton("Create");
				create.setBounds(300, 50, 100, 100);
				create.setVisible(true);

				createCustomerFrame.add(idLabel);
				createCustomerFrame.add(nameLabel);
				createCustomerFrame.add(surrnameLabel);
				createCustomerFrame.add(emailLabel);
				createCustomerFrame.add(telLabel);
				createCustomerFrame.add(nameTF);
				createCustomerFrame.add(surrnameTF);
				createCustomerFrame.add(emailTF);
				createCustomerFrame.add(telTF);
				createCustomerFrame.add(idTF);
				createCustomerFrame.add(create);

				create.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						customerLogic.newCustomer(new Customer(Integer.parseInt(idTF.getText()), nameTF.getText(),
								surrnameTF.getText(), emailTF.getText(), Long.parseLong(telTF.getText())));
						createCustomerFrame.dispose();
						if (idTF.getText() == "" || nameTF.getText() == "" || surrnameTF.getText() == ""
								|| emailTF.getText() == "" || telTF.getText() == "") {
							JOptionPane.showMessageDialog(frame, "Complete all fields!", "Error!",
									JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(frame, "Customer created succesfuly!", null,
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});
			}
		});

		btnDeleteCustomer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				deleteCustomerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				deleteCustomerFrame.setBounds(200, 200, 600, 250);
				deleteCustomerFrame.setResizable(false);
				deleteCustomerFrame.setVisible(true);
				deleteCustomerFrame.setLayout(null);

				JLabel idLabel = new JLabel("id: ");
				idLabel.setBounds(0, 10, 80, 20);
				idLabel.setVisible(true);
				idTF.setBounds(80, 10, 200, 20);
				idTF.setVisible(true);

				deleteCustomerFrame.add(idLabel);
				deleteCustomerFrame.add(idTF);

				JButton del = new JButton("Delete");
				del.setBounds(300, 50, 100, 100);
				del.setVisible(true);

				deleteCustomerFrame.add(del);

				del.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						customerLogic.deleteCustomer(Integer.parseInt(idTF.getText()));
					}
				});
			}
		});

		btnUpdateCustomer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				updateCustomerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				updateCustomerFrame.setBounds(200, 200, 600, 250);
				updateCustomerFrame.setResizable(false);
				updateCustomerFrame.setVisible(true);
				updateCustomerFrame.setLayout(null);

				JLabel idLabel = new JLabel("id: ");
				idLabel.setBounds(0, 10, 80, 20);
				idLabel.setVisible(true);
				idTF.setBounds(80, 10, 200, 20);
				idTF.setVisible(true);
				JLabel telLabel = new JLabel("tel: ");
				telLabel.setBounds(0, 50, 80, 20);
				telLabel.setVisible(true);
				telTF.setBounds(80, 50, 200, 20);
				telTF.setVisible(true);

				JButton update = new JButton("Update");
				update.setBounds(300, 50, 100, 100);
				update.setVisible(true);

				updateCustomerFrame.add(idLabel);
				updateCustomerFrame.add(idTF);
				updateCustomerFrame.add(telLabel);
				updateCustomerFrame.add(telTF);
				updateCustomerFrame.add(update);

				update.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						customerLogic.updateTelOfCustomer(Integer.parseInt(idTF.getText()),
								Integer.parseInt(telTF.getText()));
					}
				});
			}
		});

		btnListAllCustomers.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				listAllCustomers.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				listAllCustomers.setBounds(200, 200, 700, 450);
				listAllCustomers.setResizable(true);
				listAllCustomers.setVisible(true);
				listAllCustomers.setLayout(null);

				String[] arg = new String[Customer.class.getDeclaredFields().length];

				int idx = 0;
				for (Field field : Customer.class.getDeclaredFields()) {
					arg[idx++] = field.getName();
				}

				customersTable = new JTable();
				int rows = new CustomerDAObject().getNumberOfCustomers();

				@SuppressWarnings("serial")
				DefaultTableModel model = new DefaultTableModel(arg, rows) {

					@Override
					public boolean isCellEditable(int row, int column) {
						// TODO Auto-generated method stub
						return false;
					}

					public Class<?> getColumnClass(int col) {
						return String.class;
					}
				};

				int index = 0;
				for (int j = 1; j <= rows; j++) {
					Customer customer = new AbstractAccessCustomer<Customer>(Customer.class).getColumnByID(j);
					if (customer != null) {
						model.setValueAt(customer.getCustomerID(), index, 0);
						model.setValueAt(customer.getName(), index, 1);
						model.setValueAt(customer.getSurrname(), index, 2);
						model.setValueAt(customer.getEmail(), index, 3);
						model.setValueAt(customer.getTel(), index, 4);
						index++;
					}
				}

				customersTable.setModel(model);
				customersTable.setBounds(30, 50, 500, 200);
				customersTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

				DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
				cellRenderer.setHorizontalAlignment(JLabel.CENTER);
				customersTable.setCellSelectionEnabled(false);
				customersTable.setRowSelectionAllowed(true);
				customersTable.setDefaultRenderer(String.class, cellRenderer);
				customersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				customersTable.getTableHeader().setReorderingAllowed(false);
				listAllCustomers.add(customersTable);

				JScrollPane pane = new JScrollPane(customersTable);
				pane.setBounds(30, 50, 610, 200);
				pane.setVisible(true);
				listAllCustomers.add(pane);
			}
		});

		btnUpdateProduct.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				updateProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				updateProductFrame.setBounds(200, 200, 600, 250);
				updateProductFrame.setResizable(false);
				updateProductFrame.setVisible(true);
				updateProductFrame.setLayout(null);

				JLabel idProdLabel = new JLabel("id: ");
				idProdLabel.setBounds(0, 10, 80, 20);
				idProdLabel.setVisible(true);
				idProdTF.setBounds(80, 10, 200, 20);
				idProdTF.setVisible(true);
				JLabel stockProdLabel = new JLabel("stock: ");
				stockProdLabel.setBounds(0, 50, 80, 20);
				stockProdLabel.setVisible(true);
				stockProdTF.setBounds(80, 50, 200, 20);
				stockProdTF.setVisible(true);

				JButton update = new JButton("Update");
				update.setBounds(300, 50, 100, 100);
				update.setVisible(true);

				updateProductFrame.add(idProdLabel);
				updateProductFrame.add(idProdTF);
				updateProductFrame.add(stockProdLabel);
				updateProductFrame.add(stockProdTF);
				updateProductFrame.add(update);

				update.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						productLogic.updateStockOfProduct(Integer.parseInt(idProdTF.getText()),
								Integer.parseInt(stockProdTF.getText()));
					}
				});
			}
		});

		JButton btnCreateProduct = new JButton("addProduct");
		btnCreateProduct.setBounds(20, 50, 100, 20);
		frame.add(btnCreateProduct);

		JButton btnDeleteProduct = new JButton("delProduct");
		btnDeleteProduct.setBounds(170, 50, 100, 20);
		frame.add(btnDeleteProduct);

		btnCreateProduct.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				createProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				createProductFrame.setBounds(200, 200, 600, 250);
				createProductFrame.setResizable(false);
				createProductFrame.setVisible(true);
				createProductFrame.setLayout(null);

				JLabel idProdLabel = new JLabel("id: ");
				idProdLabel.setBounds(0, 10, 80, 20);
				idProdLabel.setVisible(true);
				idProdTF.setBounds(80, 10, 200, 20);
				idProdTF.setVisible(true);
				JLabel nameProdLabel = new JLabel("name: ");
				nameProdLabel.setBounds(0, 50, 80, 20);
				nameProdLabel.setVisible(true);
				nameProdTF.setBounds(80, 50, 200, 20);
				nameProdTF.setVisible(true);
				JLabel priceProdLabel = new JLabel("price: ");
				priceProdLabel.setBounds(0, 90, 80, 20);
				priceProdLabel.setVisible(true);
				priceProdTF.setBounds(80, 90, 200, 20);
				priceProdTF.setVisible(true);
				JLabel quantityProdLabel = new JLabel("quantity: ");
				quantityProdLabel.setBounds(0, 130, 80, 20);
				quantityProdLabel.setVisible(true);
				quantityProdTF.setBounds(80, 130, 200, 20);
				quantityProdTF.setVisible(true);
				JLabel stockProdLabel = new JLabel("stock: ");
				stockProdLabel.setBounds(0, 170, 80, 20);
				stockProdLabel.setVisible(true);
				stockProdTF.setBounds(80, 170, 200, 20);
				stockProdTF.setVisible(true);

				JButton create = new JButton("Create");
				create.setBounds(300, 50, 100, 100);
				create.setVisible(true);

				createProductFrame.add(idProdLabel);
				createProductFrame.add(nameProdLabel);
				createProductFrame.add(priceProdLabel);
				createProductFrame.add(quantityProdLabel);
				createProductFrame.add(stockProdLabel);
				createProductFrame.add(nameProdTF);
				createProductFrame.add(stockProdTF);
				createProductFrame.add(priceProdTF);
				createProductFrame.add(quantityProdTF);
				createProductFrame.add(idProdTF);
				createProductFrame.add(create);

				create.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						productLogic.newProduct(new Product(Integer.parseInt(idProdTF.getText()), nameProdTF.getText(),
								Integer.parseInt(priceProdTF.getText()), Integer.parseInt(quantityProdTF.getText()),
								Integer.parseInt((stockProdTF.getText()))));
						System.out.println("Product created succesfuly!");
					}
				});
			}
		});
		btnDeleteProduct.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				deleteProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				deleteProductFrame.setBounds(200, 200, 600, 250);
				deleteProductFrame.setResizable(false);
				deleteProductFrame.setVisible(true);
				deleteProductFrame.setLayout(null);

				JLabel idProdLabel = new JLabel("id: ");
				idProdLabel.setBounds(0, 10, 80, 20);
				idProdLabel.setVisible(true);
				idProdTF.setBounds(80, 10, 200, 20);
				idProdTF.setVisible(true);

				deleteProductFrame.add(idProdLabel);
				deleteProductFrame.add(idProdTF);

				JButton del = new JButton("Delete");
				del.setBounds(300, 50, 100, 100);
				del.setVisible(true);

				deleteProductFrame.add(del);

				del.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						productLogic.deleteProduct(Integer.parseInt(idProdTF.getText()));
					}
				});
			}
		});

		btnListAllProducts.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				listAllProducts.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				listAllProducts.setBounds(200, 200, 700, 450);
				listAllProducts.setResizable(true);
				listAllProducts.setVisible(true);
				listAllProducts.setLayout(null);

				String[] arg = new String[Product.class.getDeclaredFields().length];

				int idx = 0;
				for (Field field : Product.class.getDeclaredFields()) {
					arg[idx++] = field.getName();
				}

				productsTable = new JTable();
				int rows = new ProductDAObject().getNumberOfProducts();

				@SuppressWarnings("serial")
				DefaultTableModel model = new DefaultTableModel(arg, rows) {

					@Override
					public boolean isCellEditable(int row, int column) {
						// TODO Auto-generated method stub
						return false;
					}

					public Class<?> getColumnClass(int col) {
						return String.class;
					}
				};

				int index = 0;
				for (int j = 1; j <= rows; j++) {
					Product product = new AbstractAccess<>(Product.class).getColumnByID(j);
					if (!product.getName().equals("null")) {
						model.setValueAt(product.getId(), index, 0);
						model.setValueAt(product.getName(), index, 1);
						model.setValueAt(product.getStock(), index, 2);
						model.setValueAt(product.getPrice(), index, 3);
						model.setValueAt(product.getQuantity(), index, 4);
						index++;
					}
				}

				productsTable.setModel(model);
				productsTable.setBounds(30, 50, 500, 200);
				productsTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

				DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
				cellRenderer.setHorizontalAlignment(JLabel.CENTER);
				productsTable.setCellSelectionEnabled(false);
				productsTable.setRowSelectionAllowed(true);
				productsTable.setDefaultRenderer(String.class, cellRenderer);
				productsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				productsTable.getTableHeader().setReorderingAllowed(false);
				listAllProducts.add(productsTable);

				JScrollPane pane = new JScrollPane(productsTable);
				pane.setBounds(30, 50, 610, 200);
				pane.setVisible(true);
				listAllProducts.add(pane);
			}
		});

		btnOrders.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				orderFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				orderFrame.setBounds(200, 200, 800, 350);
				orderFrame.setResizable(false);
				orderFrame.setVisible(true);
				orderFrame.setLayout(null);

				JButton finishOrd = new JButton("Place");
				finishOrd.setBounds(20, 280, 100, 20);
				orderFrame.add(finishOrd);

				String[] arg = new String[Product.class.getDeclaredFields().length + 1];

				int idx = 0;
				for (Field field : Product.class.getDeclaredFields()) {
					arg[idx++] = field.getName();
				}
				arg[idx] = "Cantitate";

				productsTable = new JTable();
				int rows = new ProductDAObject().getNumberOfProducts();

				@SuppressWarnings("serial")
				DefaultTableModel model = new DefaultTableModel(arg, rows) {

					@Override
					public boolean isCellEditable(int row, int column) {
						// TODO Auto-generated method stub
						if (column == 5) {
							return true;
						}
						return false;
					}

					public Class<?> getColumnClass(int col) {
						return String.class;
					}
				};

				int index = 0;
				for (int j = 1; j <= rows; j++) {
					Product product = new AbstractAccess<>(Product.class).getColumnByID(j);
					if (!product.getName().equals("null")) {
						model.setValueAt(product.getId(), index, 0);
						model.setValueAt(product.getName(), index, 1);
						model.setValueAt(product.getPrice(), index, 2);
						model.setValueAt(product.getQuantity(), index, 3);
						model.setValueAt(product.getStock(), index, 4);
						model.setValueAt(0, index, 5);
						index++;
					}
				}

				productsTable.setModel(model);
				productsTable.setBounds(30, 50, 500, 200);
				productsTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

				DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
				cellRenderer.setHorizontalAlignment(JLabel.CENTER);
				productsTable.setCellSelectionEnabled(false);
				productsTable.setRowSelectionAllowed(true);
				productsTable.setDefaultRenderer(String.class, cellRenderer);
				productsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				productsTable.getTableHeader().setReorderingAllowed(false);
				orderFrame.add(productsTable);

				final JTextField CustIDForOrder = new JTextField();
				CustIDForOrder.setBounds(80, 10, 20, 20);
				CustIDForOrder.setVisible(true);
				orderFrame.add(CustIDForOrder);

				JScrollPane pane = new JScrollPane(productsTable);
				pane.setBounds(30, 50, 610, 200);
				pane.setVisible(true);
				orderFrame.add(pane);

				finishOrd.addActionListener(new ActionListener() {
					int idO = 0;

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						int length = new ProductDAObject().getNumberOfProducts();

						for (int i = 1; i <= length; i++) {
							int cantitate = Integer.parseInt(productsTable.getModel().getValueAt(i - 1, 5).toString());
							Product product = new AbstractAccess<>(Product.class).getColumnByID(i);
							if (cantitate > 0) {
								if (cantitate > product.getStock()) {
									JOptionPane.showMessageDialog(orderFrame, "Stoc Epuizat!", "Eroare",
											JOptionPane.ERROR_MESSAGE);
								} else {
									OrderProduct produsComandat = new OrderProduct(idO++, 0, product.getId(),
											cantitate);
									orderProducts.add(produsComandat);
								}
							}
						}
						new OrderProductDAObject().addOrder(Integer.parseInt(CustIDForOrder.getText().toString()),
								orderProducts);
						new OrderProductDAObject().createBill(Integer.parseInt(CustIDForOrder.getText().toString()),
								orderProducts);
					}
				});
			}
		});
	}

	public static void main(String[] args) throws SQLException {
		CustomerGUI show = new CustomerGUI();
		show.init();
	}

}
